@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              @if($ClientProperty->client4=="" || $ClientProperty->client3=="" || $ClientProperty->client2=="" || $ClientProperty->client4=="")

                <div class="card-header"><h2> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName }}'s Properties</h2>

              @else
                  <div class="card-header"><h3> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName . " ," . $ClientProperty->client4 . " ," . $ClientProperty->client3. " , and " . $ClientProperty->client2 }}'s Properties</h3>
              @endif
                  <h4>{{$ClientProperty->address}}</h4>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Client Property</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">block</th>
      <th scope="col">lot</th>
        <th scope="col">TCP</th>
        <th scope="col">Monthly Amortization</th>
        <th scope="col">Total Paid</th>
        <th scope="col">Balance</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>



@foreach($clientProperties as $items)

<tr>

  <th scope="row" align="center">{{$count++}}</th>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->block}}</td>

  <td align="center">{{$items->lot}}</td>
  <td align="center">{{number_format(round($items->contractPrice,2),2,'.',',')}}</td>
  <td align="center">{{number_format(round($items->monthlyAmortization,2),2,'.',',')}}  </td>
  <td align="center">{{number_format(round($items->totalPaid ,2),2,'.',',') }}</td>
  <td align="center">{{number_format(round((($items->contractPrice - $items->totalPaid)) ,2),2,'.',',') }}</td>


  <td align="center">
    <a class="btn btn-sm btn-outline-secondary" id="getRequest" href="{{'/forfeit/'.$items->cp_id.'/'.$items->propertylistid}}" role="button">Forfeit</a>
      <a class="btn btn-sm btn-outline-primary" href="#" role="button">Assume</a>
    <a class="btn btn-sm btn-outline-success" href="{{'/paymenthistory/'.$items->cp_id}}" role="button">Payment History</a>

</td>
</tr>

@endforeach
</tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="addProperty" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content ">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Client Property</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/clientProperties/create">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">Select Property</label>
               <select class="form-control" name="property" id="property">
                  <option value="">Select Property</option>
                  @foreach($propertyNameList as $plist)
                 <option value="{{$plist->propId}}">{{$plist->propertyName}}</option>
                @endforeach
               </select>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Block</label>
               <select class="form-control" name="pList" id="pList">
                 <option value="">Select Block</option>

               </select>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Lot</label>
               <select class="form-control" name="lotList" id="lotList">
                 <option value="">Select Lot</option>

               </select>
             </div>
           </div>

           <div class="form-row">

              <div class="form-group col-md-4">
                <label for="product_name">Total Contract Price</label>
                <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                <small id="asNameError" class="form-text text-muted"></small>

              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">PlanTerms</label>
              <input type="text" class="form-control" id="terms" name="terms" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="product_name">Monthly Amortization</label>
                  <input type="text" class="form-control" id="Monthly" name="Monthly">
                    <input type="hidden" id="cid" name="cid" value="{{$ClientProperty->client_id }}">
                <small id="asNameError" class="form-text text-muted"></small>
              </div>
            </div>
             <div class="form-row">
               <div class="form-group col-md-4 mx-auto">
                 <label for="Agent_name">Agent</label>

                 <select name="agent" style="width:240px;" id="agent">
                    <option value="">Select AGent</option>

                 </select>

                 <small id="asNameError" class="form-text text-muted"></small>

               </div>
               <div class="form-group col-md-2 mx-auto">
                 <label for="product_name">Comm%</label>

                   <select id="comper" name="comper" class="form-control">
                     <option value="0.07"> 7% </option>
                     <option value="0.08"> 8% </option>
                     <option value="0.09"> 9% </option>
                     <option value="0.10"> 10% </option>
                   </select>
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-3 mx-auto">
                 <label for="product_name">Reservation</label>
                   <input type="text" class="form-control" id="res" name="res" value="0">
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-3 mx-auto">
                 <label for="product_name">Or Number</label>
                   <input type="text" class="form-control" id="OR" name="OR" value="000-0000-000">
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>

               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Due Date</label>
                   <input type="text" class="form-control" id="due" name="due">
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Commission Release</label>
                   <input type="text" class="form-control" id="comRel" name="comRel">
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Monthly Commission</label>
                   <input type="text" class="form-control" id="monCOm" name="monCOm" readonly>
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>

               </div>
               <div class="form-row">
                 <div class="form-group col-md-4 mx-auto">
                   <label for="product_name">Date</label>
                     <input type="date" class="form-control" id="date" name="date" >
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                     <label for="Unit">SubAgent</label>
                     <select class="form-control" name="downline" id="downline">
                       <option value="">Select Property</option>

                     </select>
                   </div>
                 </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#property').on('change', function () {
                 let prop_id = $(this).val();
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Processing...</option>`);
                 $.ajax({
                 type: 'GET',
                 url: '/propertyLists/' + prop_id,
                 success: function (response) {
                 var response = JSON.parse(response);
                 // console.log(response);
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Select Block*</option>`);
                 response.forEach(element => {
                     $('#pList').append(`<option value="${element['block']}">Block ${element['block']}</option>`);
                     });
                 }
             });
         });
         $('#pList').on('change', function () {
                        let block = $(this).val();
                        var prop_id = $('#property').val();
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Processing...</option>`);
                        $.ajax({
                        type: 'GET',
                        url: '/propertyListsActive/' + block +'/'+ prop_id ,
                        success: function (response) {
                        var response = JSON.parse(response);
                        // console.log(response);
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                        response.forEach(element => {
                            $('#lotList').append(`<option value="${element['propertylistid']}">Lot ${element['lot']}</option>`);
                            });
                        }
                    });
                });
                $('#lotList').on('change', function () {
                               let propertylist = $(this).val();
                               $.ajax({
                               type: 'GET',
                               dataType: 'json',
                               url: '/propertyListsView/' + propertylist ,
                               success: function (data) {
                               // var data = JSON.parse(data);
                               console.log(data[0]);
                                $('#contractPrice').val(data[0].contractPrice);
                               //alert(data[0].contractPrice);


                               }
                           });
                       });
                       $('#comRel').on('change', function () {
                                      let release = $(this).val();
                              let price = $('#contractPrice').val();
                              let percent = $('#comper').val();
                              let com = (price*percent)/release;
                              $('#monCOm').val(com);
                              });

                       $('#res').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });
                       $('#terms').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });
                      $('#agent').select2({
                        ajax:{
                          url:"/agent/getList",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                            _token:CSRF_TOKEN,
                            search: params.term
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#agent').on('change', function () {
                                   let agent_id = $(this).val();
                                   $('#downline').empty();
                                   $('#downline').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/getsubagentlist/' + agent_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#downline').empty();
                                   $('#downline').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element => {

                                     // console.log(element.data.SubAgentName);
                                       $('#downline').append(`<option value="${element.id}"> ${element.SubAgentName} </option>`);
                                       });
                                   }
                               });
                           });

});
</script>
@endsection
