<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function index()
     {
         return view('clients.clientlist');
     }
     public function clientlists(Request $request)
     {
      $data = DB::table('clients')->get();
       return view('clients.clientlist',['data'=>$data])->with('count',1);
     }
     public function store(Request $request)
     {
       $request->validate([
         'firstName' => 'required',
         'lastName' => 'required',
         'mobileNumber' => 'required',


       ]);
      $client = new Client();
      $client->firstName = $request->input('firstName');
      $client->middleName = $request->input('middleName');
      $client->lastName = $request->input('lastName');
      $client->birthDate = $request->input('birthDate');
      $client->client2 = $request->input('Client2');
      $client->client3 = $request->input('Client3');
      $client->client4 = $request->input('Client4');
      $client->placeofBirth = $request->input('placeofBirth');
      $client->civilStatus = $request->input('civilStatus');
      $client->mobileNumber = $request->input('mobileNumber');
      $client->emailAddress = $request->input('emailAddress');
      $client->address = $request->input('address');
      $client->gender = $request->input('gender');
      $client->referenceName = $request->input('referenceName');
      $client->referenceAddress = $request->input('referenceAddress');
      $client->referenceContact = $request->input('referenceContact');
      $client->save();
      return redirect()->back()->with('message', 'New Client Added Successfully');


       // return view('property.propertylist');
     }
     public function edit(Request $request)
     {

       $id = $request->input('client_id');
       $fname = $request->input('EditfirstName');
       $mname = $request->input('EditmiddleName');
       $mname = $request->input('EditmiddleName');
       $lname = $request->input('EditlastName');
       $address = $request->input('Editaddress');
       $number = $request->input('EditmobileNumber');
       $status = $request->input('EditcivilStatus');




         $data1=Client::find($id);
         $data1->firstName=$fname;
         $data1->middleName=$mname;
         $data1->lastName=$lname;
         $data1->address=$address;
         $data1->mobileNumber=$number;
         $data1->civilStatus=$status;
         $data1->save();


       return redirect()->back()->with('message', 'Client Edited Successfully');
     }
     public function getClientList(Request $request)
     {
       $search = $request->search;
       if ($search=='') {
         $Clients = Client::orderBy('lastName', 'asc')
         ->select('client_id', DB::raw('CONCAT(firstName, " ", lastName) AS full_name'))
         ->limit(5)
         ->get();
       }
       else {
         $Clients = Client::orderBy('lastName', 'asc')
         ->select('client_id', DB::raw('CONCAT(firstName, " ", lastName) AS full_name'))
         ->where('firstName', 'like','%'.$search.'%' )
         ->orWhere('lastName', 'like','%'.$search.'%' )
         ->limit(5)
         ->get();
 }

       $responses = array();
       foreach ($Clients as $client) {
         $responses[] = array(
           "id"=>$client->client_id,
           "text"=>$client->full_name
         );
       }

       echo json_encode($responses);
       exit;
     }
}
