<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use DataTables;

class PropertyController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function proplist(Request $request)
  {
    //  $id = Auth::id();
    // $email = DB::table('users')
    // ->select('email')
    // ->where('id',$id)
    // ->get();
    // dd($email);
   $data = DB::table('properties')->paginate(100);
    return view('property.propertylist',['data'=>$data])->with('count',1);
  }
  public function EditProp(Request $request)
  {

    $id = $request->input('id');
    $name = $request->input('propNamez');
    $add = $request->input('addressz');


      $data1=Property::find($id);
      $data1->propertyName=$name;
      $data1->address=$add;
      $data1->save();


    return redirect()->back()->with('message', 'Property Edited Successfully');
  }
  public function store(Request $request)
  {

  $request->validate([
    'propertyName' => 'required',
    'address' => 'required',
  ]);
    Property::create($request->all());
  return redirect()->back()->with('message', 'Parent Property Added Successfully');


    // return view('property.propertylist');
  }
}
