<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
  public function index()
  {

    // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

    return view('reports.generatereport');
     // return view('property.newproperty',['data'=>$data])->with('count',1);
    // return $propId;
  }

  public function agents()
  {
      $pdf = new \FPDF();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Ln();
      $users = \App\Models\User::all();
      foreach($users as $user) {
          $pdf->Cell(40, 10, $user->name, 1);
          $pdf->Ln();
      }
      $pdf->Output();
      exit;
  }
  public function statement($cp_id)
  {
      $pdf = new \FPDF();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Ln();
      $data = DB::table('payments')
      ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
      ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select('payments.otherpayment','payments.payment','payments.paymentName', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','client__properties.agent_id')
      ->where('client__properties.cp_id', '=', $cp_id)
      ->get();
      foreach($data as $items) {
          $pdf->Cell(40, 10, $items->paymentName, 1);
          $pdf->Ln();
      }
      $pdf->Output();
      exit;
  }
}
