<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client_Property;
use App\Models\Client;
use App\Models\Propertylist;
use App\Models\Property;
use App\Models\Payment;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClientPropertyController extends Controller
{
    //

    Public function getpropList(Request $request)
    {
      $propID = $request->prop_id;
   $data = DB::table('propertylists')

      ->where('propertylists.propId',$propID)
      ->get();
// dd($data);
    }
    Public function forfeit($cp_id,$propertylistid)
    {

        $data=Client_Property::find($cp_id);
        $data->isActive=0;
        $data->save();
        $data2=Propertylist::find($propertylistid);
        $data2->client_id=NULL;
        $data2->status=1;
        $data2->save();
 return redirect()->back()->with('message', 'Property Forfeited Successfully');
    }

    Public function getActiveListPay($client_id)
    {


      // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

$propertyNameList = DB::table('properties')->get();
$clientProperties = Propertylist::with(['getpropertylistRelation','getClientpropertyRelation'])
->whereHas('getClientpropertyRelation', function($q) use($client_id) {
    // Query the name field in status table
    $q->where('client_id', '=', $client_id); // '=' is optional
})
->whereHas('getClientpropertyRelation', function($q) use($client_id) {
    // Query the name field in status table
    $q->where('isActive', '=', 1); // '=' is optional
})

->get();

// dd($clientProperties);
echo json_encode($clientProperties);

      // $data = DB::table('client__properties')->where('client_id', $client_id)->paginate(10);
    //  dd($clientProperties);

       // return view('property.newproperty',['data'=>$data])->with('count',1);
      // return $propId;
    }
    Public function Fetch($cp_id)
    {

$clientProperties = Propertylist::with(['getpropertylistRelation','getClientpropertyRelation'])
->whereHas('getClientpropertyRelation', function($q) use($cp_id) {
    // Query the name field in status table
    $q->where('cp_id', '=', $cp_id); // '=' is optional
})
->get();
$propertyNameList = DB::table('properties')->get();

echo json_encode ($clientProperties);

    }
    Public function clientProperties($client_id)
    {
      $ClientProperty = Client::find($client_id);

      // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

$propertyNameList = DB::table('properties')->get();

$clientProperties = DB::table('client__properties')
->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
->join('properties', 'properties.propId', '=', 'propertylists.propId')
->select('client__properties.cp_id','client__properties.Commission','client__properties.totalPaid','client__properties.monthlyAmortization','propertylists.contractPrice','propertylists.propertylistid','properties.propertyName','propertylists.lot','propertylists.block')
->where('client__properties.client_id', '=', $client_id)
->where('client__properties.isActive', '=', 1)
->get();



      // $data = DB::table('client__properties')->where('client_id', $client_id)->paginate(10);
    //  dd($clientProperties);
      return view('clientProperties.clientPropertyList', compact('ClientProperty', 'clientProperties','propertyNameList'))->with('count',1);
       // return view('property.newproperty',['data'=>$data])->with('count',1);
      // return $propId;
    }
    public function store(Request $request)
    {
      $request->validate([
        'agent' => 'required',
        'terms' => 'required',
        'due'  => 'required',
        'Monthly' => 'required' ,
        'lotList' => 'required',
        'cid'=>'required',


      ]);
      $commRell = $request->input('comRel') * $request->input('monCOm');
      if ($request->input('res')>0) {
    $finalCom = ($request->input('comRel'))-1;
      }
      else{
        $finalCom = ($request->input('comRel'));
      }

     $client = new Client_Property();
     $client->agent_id = $request->input('agent');
     $client->PlanTerms = $request->input('terms');
     $client->dueDate = $request->input('due');
     $client->client_id = $request->input('cid');
     $client->monthlyAmortization = $request->input('Monthly');
     $client->propertylistid = $request->input('lotList');
     $client->Commission = $request->input('monCOm');
     $client->totalPaid = $request->input('res');
      $client->comRelease = $finalCom;
      $client->commTotal = $commRell;
      $client->subAgent = $request->input('downline');

      $client->created_at =$request->input('date');
     $client->save();

    $cp_id = $client->cp_id;
     if ($request->input('res')>0) {
     $this->storePayment($request->input('res'),$request->input('OR'),$request->input('date'),$request->input('monCOm'),$cp_id);
     }
      $this->updatePropertyStatus($request->input('lotList'),$request->input('cid'));
     return redirect()->back()->with('message', 'Property Added Successfully');


      // return view('property.propertylist');
    }
    private function updatePropertyStatus($propertylistid,$client_id)
    {
      $data=Propertylist::find($propertylistid);
      $data->status='0';
      $data->client_id = $client_id;
      $data->save();
    }
    private function storePayment($res,$OR,$date,$commission,$cp_id)
    {

      $id = Auth::id();
     $branch = DB::table('users')
     ->select('branch')
     ->where('id',$id)
     ->get();
     $finalBranch = ($branch[0]->branch);
      $pay = new Payment();
  $pay->user_id =  $id;
  $pay->branch = $finalBranch;
  $pay->Commission = $commission;
  $pay->or_num = $OR;
  $pay->cp_id = $cp_id;
  $pay->payment =$res;
  $pay->otherpayment = 0;
  $pay->penalty = 0;
  $pay->paymentName = 'Reservation';
  $pay->paymentMethod = 'Cash';
  $pay->paymentDesc = 'Payment for Reservation';
  $pay->created_at =$date;
  $pay->save();
    }
}
