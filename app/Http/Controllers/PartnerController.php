<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\partner;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
  public function partnerslist(Request $request)
  {
   $data = DB::table('partners')->paginate(100);
    return view('partners.partnerslist',['data'=>$data])->with('count',1);
  }
  public function store(Request $request)
  {
    $request->validate([
      'PartnerName' => 'required',
      'PartnerAddress' => 'required',
      'PartnerContact' => 'required',
    ]);
      Partner::create($request->all());
    return redirect()->back()->with('message', 'Partner Added Successfully');

  }
  public function edit(Request $request)
  {

    $id = $request->input('id');
    $name = $request->input('PartnerNameEdit');
    $add = $request->input('PartnerAddressEdit');
    $contact = $request->input('PartnerContactEdit');


      $data1=partner::find($id);
      $data1->PartnerName=$name;
      $data1->PartnerAddress=$add;
      $data1->PartnerContact=$contact;
      $data1->save();


    return redirect()->back()->with('message', 'Partner Edited Successfully');
  }

}
