<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agent;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use DataTables;

class AgentController extends Controller
{
    //
    public function store(Request $request)
    {
    $request->validate([
      'AgentFname' => 'required',
      'AgentLname' => 'required',
      'AgentAddress' => 'required',


    ]);
      Agent::create($request->all());
    return redirect()->back()->with('message', 'An Agent Added Successfully');
  }
  public function edit(Request $request)
  {

    $id = $request->input('editID');
    $fname = $request->input('EditAgentFname');
    $mname = $request->input('EditAgentMname');
    $lname = $request->input('EditAgentLname');
    $address = $request->input('EditAgentAddress');
    $email = $request->input('EditAgentEmailAd');
    $mobile = $request->input('EditAgentMobile');




      $data1=Agent::find($id);
      $data1->AgentFname=$fname;
      $data1->AgentMname=$mname;
      $data1->AgentLname=$lname;
      $data1->AgentAddress=$address;
      $data1->AgentMobile=$mobile;
      $data1->AgentEmailAd=$email;
      $data1->save();


    return redirect()->back()->with('message', 'Agent Edited Successfully');
  }
    public function agentList(Request $request)
    {
     $data = DB::table('agents')->get();
     return view('agents.agentlist',['data'=>$data])->with('count',1);
    }
    // public function store(Request $request)
    // {
    // $request->validate([
    //   'AgentFname' => 'required',
    //   'AgentLname' => 'required',
    //   'AgentAddress' => 'required',
    //   'AgentMobile' => 'required|max:11|min:11',
    //
    // ]);
    //   Agent::create($request->all());
    // return redirect()->back()->with('message', 'An Agent Added Successfully');


      // return view('property.propertylist');

    ##AJAX function
    public function getAgents(Request $request)
    {
      $search = $request->search;
      if ($search=='') {
        $Agents = Agent::orderBy('AgentLname', 'asc')
        ->select('agent_id', DB::raw('CONCAT(AgentFname, " ", AgentLname) AS full_name'))
        ->limit(5)
        ->get();
      }
      else {
        $Agents = Agent::orderBy('AgentLname', 'asc')
        ->select('agent_id', DB::raw('CONCAT(AgentFname, " ", AgentLname) AS full_name'))
        ->where('AgentLname', 'like','%'.$search.'%' )
        ->orWhere('AgentFname', 'like','%'.$search.'%' )
        ->limit(5)
        ->get();
}

      $response = array();
      foreach ($Agents as $agent) {
        $response[] = array(
          "id"=>$agent->agent_id,
          "text"=>$agent->full_name
        );
      }

      echo json_encode($response);
      exit;
    }
}
