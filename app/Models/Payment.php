<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

      protected $fillable = ['user_id', 'payment','penalty','paymentName','paymentMethod','paymentDesc','branch','or_num','Commission','releaseDate','created_at','commTotal'];
      public function getAgentRelation()
      {
        return $this->hasOne('App\Models\Agent', 'agent_id', 'agent_id');
      }
      public function getCLientProperty()
      {
        return $this->belongsTo('App\Models\Client_Property', 'cp_id', 'cp_id');
      }
}
