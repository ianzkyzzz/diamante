<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Client extends Model
{
    use HasFactory;
protected $primaryKey = 'client_id';
protected $appends = ['age'];
protected $fillable = ['firstName', 'middleName','lastName','birthDate', 'placeofBirth', 'civilStatus', 'mobileNumber', 'emailAddress', 'address', 'referenceName', 'referenceAddress', 'referenceContact','gender'];
public function getbirthDateAttribute()
{
   return Carbon::parse($this->attributes['birthDate'])->age;
}
}
