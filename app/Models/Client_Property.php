<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client_Property extends Model
{
    use HasFactory;
    protected $fillable = ['agent_id', 'PlanTerm','dueDate','monthlyAmortization' ,'propertylistid','comRelease','subAgent','created_at'];

protected $primaryKey = 'cp_id';

}
