<?php

use Illuminate\Support\Facades\Route;
// use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\PropertylistController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home');
Route::get('/home', function () {
    return view('dashboard');
});
Route::get('/users', 'App\Http\Controllers\UserController@index');
// Route::get('/hjhj', function () {
//     return 'Hello world';

// Route::get('/aboutus', [PropertyController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home');

Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'uploadAvatar']);
Route::post('/upload', [UserController::class,'uploadAvatar']);

Route::get('/home', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home');
Route::get('/property', [App\Http\Controllers\PropertyController::class, 'proplist']);
Route::post('/property/create', [App\Http\Controllers\PropertyController::class, 'store']);
Route::get('/propertylist', [App\Http\Controllers\PropertyController::class, 'propertylist']);
Route::get('/propertyList/{propId}/propertylists', [App\Http\Controllers\PropertylistController::class, 'propertyListAll']);
Route::post('/propertyList/create', [App\Http\Controllers\PropertylistController::class, 'storepropertylist']);
Route::get('/client', [App\Http\Controllers\ClientController::class, 'clientlists'])->name('client');
Route::post('/client/create', [App\Http\Controllers\ClientController::class, 'store'])->name('client');
Route::post('/client/Edit', [App\Http\Controllers\ClientController::class, 'edit']);
Route::get('/agent', [App\Http\Controllers\AgentController::class, 'agentList'])->name('Agent');
Route::post('/agent/create', [App\Http\Controllers\AgentController::class, 'store'])->name('Agent');
Route::post('/agent/Edit', [App\Http\Controllers\AgentController::class, 'edit']);
Route::get('/clientProperties/{propId}/list', [App\Http\Controllers\ClientPropertyController::class, 'clientProperties'])->name('clientProperties');
Route::get('/clientProperties/getList', [App\Http\Controllers\ClientPropertyController::class, 'getpropList'])->name('client');
Route::get('/propertyLists/{prop_id}', [App\Http\Controllers\PropertylistController::class, 'getBlockList']);
Route::get('/propertyListsActive/{block}/{prop_id}', [App\Http\Controllers\PropertylistController::class, 'getActiveList']);
Route::get('/propertyListsView/{propertylist}', [App\Http\Controllers\PropertylistController::class, 'getView']);
Route::post('/agent/getList', [App\Http\Controllers\AgentController::class, 'getAgents'])->name('Agent.getAgents');
Route::post('/clientProperties/create', [App\Http\Controllers\ClientPropertyController::class, 'store'])->name('clientProperties.store');
Route::get('/Payment', [App\Http\Controllers\PaymentController::class, 'index'])->name('Payment.index');
Route::get('/OtherPayment', [App\Http\Controllers\PaymentController::class, 'otherindex'])->name('Payment.otherindex');
Route::post('/client/getList', [App\Http\Controllers\ClientController::class, 'getClientList'])->name('client.getClientList');
Route::get('/clientPropertiesList/{client_id}', [App\Http\Controllers\ClientPropertyController::class, 'getActiveListPay']);
Route::get('/clientPropertyFetch/{cp_id}', [App\Http\Controllers\ClientPropertyController::class, 'Fetch'])->name('clientProperty.Fetch');
Route::post('payment/store', [App\Http\Controllers\PaymentController::class, 'Save'])->name('payment.Save');
Route::post('/propertyList/Blockcreate', [App\Http\Controllers\PropertylistController::class, 'storepropertylistmany']);
Route::post('/propertyList/edit', [App\Http\Controllers\PropertylistController::class, 'Edit']);
Route::get('/test', [App\Http\Controllers\PropertylistController::class, 'index']);
Route::get('/Commission/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getUnclaimed']);
Route::get('/CommissionHistory/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getRelease']);
Route::get('/CommissionHistoryDate/{agent_id}/{relDate}', [App\Http\Controllers\PaymentController::class, 'getReleaseDate']);
Route::get('/CommissionHistoryClient/{agent_id}/{client}', [App\Http\Controllers\PaymentController::class, 'getReleaseClient']);

Route::get('/paymenthistory/{cp_id}', [App\Http\Controllers\PaymentController::class, 'listPaid']);
Route::get('/userlist', [App\Http\Controllers\UserController::class, 'userlists']);
Route::get('/newUser', [App\Http\Controllers\UserController::class, 'loaduserform']);
Route::post('/newUseradd', [App\Http\Controllers\UserController::class, 'adduser']);
Route::post('/otherpayment/store', [App\Http\Controllers\PaymentController::class, 'SaveOtherPayment'])->name('payment.SaveOtherPayment');
Route::get('/expenses', [App\Http\Controllers\ExpenseController::class, 'index']);
Route::post('/expense/add', [App\Http\Controllers\ExpenseController::class, 'store']);
Route::get('/userprofile', [App\Http\Controllers\UserController::class, 'profile']);
Route::post('/user/update', [App\Http\Controllers\UserController::class, 'editUser']);
Route::get('/reports', [App\Http\Controllers\ReportController::class, 'index']);
Route::get('/getpropListwithblock/{prop}/{block}', [App\Http\Controllers\PropertylistController::class, 'propertylistwithBlock']);
Route::get('/Release/{id}', [App\Http\Controllers\PaymentController::class, 'releaseComm']);
Route::get('/ReleaseDate/{agent_id}/{date}', [App\Http\Controllers\PaymentController::class, 'releaseCommDate']);
Route::get('/forfeit/{cp_id}/{propertylistid}', [App\Http\Controllers\ClientPropertyController::class, 'forfeit']);
Route::get('/paymenthistory', [App\Http\Controllers\PaymentController::class, 'paymenthistory']);
Route::post('/paymentEdit', [App\Http\Controllers\PaymentController::class, 'Edit'])->name('payment.Edit');
Route::post('/propertyedit', [App\Http\Controllers\PropertyController::class, 'EditProp']);
Route::get('/paymentDelete/{id}', [App\Http\Controllers\PaymentController::class, 'unlist']);
Route::get('/SubAgentList/{agent_id}', [App\Http\Controllers\SubagentController::class, 'fetch']);
Route::post('/subagent/create', [App\Http\Controllers\SubagentController::class, 'store'])->name('SubAgent');
Route::get('/test', [App\Http\Controllers\PaymentController::class, 'scheduleSMS']);
Route::get('/getsubagentlist/{agent_id}', [App\Http\Controllers\SubagentController::class, 'getSubagent']);


Route::group([
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::get('SOA/{cp_id}', 'ReportController@statement');
});


Route::group([
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::get('reports/agents', 'ReportController@agents');
});


// Route::post('/registerNow', [App\Http\Controllers\UserController::class, 'adduser']);



//Route::resource('property', 'PropertyController');
//Route::resource('client', 'App\Http\Controllers\ClientController');
