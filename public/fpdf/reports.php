<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');
    $this->Ln(25);

    $this->Image('dias.png',84,0,-250);
    $this->setFont("Arial",'B',12);
    $this->Cell(201,3,"Diamante Realty.",0,1,"C");
    $this->Ln(1);
     $this->setFont("Arial",'',8);
    $this->Cell(201,3,"Purok Puso, Cabaluna Street, Barangay Gredu,",0,1,"C");
    $this->Cell(201,3,"Panabo City, 8015, Davao del Norte, Philippines 8000",0,1,"C");
    $this->Cell(201,3,"Tel no. (084) 309 1947",0,1,"C");
     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(4);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'Diamante Realty',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '2n.QL8(TeB}qGk?4';//2n.QL8(TeB}qGk?4 for testing live
         $dbname = 'diamante_db';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
$red = $_GET['from'];
        $pdf->SetFont('Times','B',10);
        $pdf->Cell(0,5,'Sales for Period:  ' .  date("F j, Y", strtotime($_GET['from'])) . " to " .  date("F j, Y", strtotime($_GET['to'])) ,0,1,'L');
        $pdf->Ln(2);
        $pdf->Cell(0,5,'CASH:',0,1,'L');
        $pdf->Ln(2);
$pdf->Cell(5,5,"#",1,0,"C");
    $pdf->Cell(36.5,5,"Payments",1,0,"C");
    $pdf->Cell(26.5,5,"OR #",1,0,"C");
    $pdf->Cell(36.5,5,"Client",1,0,"C");
    $pdf->Cell(36.5,5,"Property",1,0,"C");

    $pdf->Cell(56.5,5,"Description",1,1,"C");
    $pdf->SetFont('Times','',10);

         $sql = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`,  prop.`propertyName`, plist.`block`, plist.`lot`, pay.`payment`, pay.`paymentDesc` FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND pay.`paymentMethod`='Cash' AND (pay.`created_at` BETWEEN '".$_GET['from']."' AND '".$_GET['to']."');
";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {



                $total= $total + $row["payment"] ;



                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                $pdf->Cell(36.5,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
                $pdf->Cell(26.5,5,$row["or_num"],0,0,"C");
                $pdf->Cell(36.5,5,$row["rname"],0,0,"C");
                $pdf->Cell(36.5,5,$row["propertyName"] ,0,0,"C");

                $pdf->Cell(56.5,5,$row["paymentDesc"],0,1,"C");




                }

  $pdf->Ln(10);


         } else {
             $pdf->Cell(196,5,"NO PAYMENTS YET!!",1,1,"C");
         }
         $pdf->SetFont('Times','B',10);
       $pdf->Cell(5,5,"",0,0,"C");
  $pdf->Cell(26.5,5,"",0,0,"C");
  $pdf->Cell(36.5,5,"",0,0,"C");
  $pdf->Cell(56.5,5,"",0,0,"C");
    $pdf->SetFont('Times','',15);
  $pdf->Cell(73,5,"CASH TOTAL: ".number_format(round($total,2),2,'.',',')." "  ,0,1,"C");
  $pdf->SetFont('Times','',10);
$pdf->Ln(10);
         $pdf->SetFont('Times','B',10);
         $pdf->Cell(0,5,'BANK:',0,1,'L');
         $pdf->Ln(2);
 $pdf->Cell(5,5,"#",1,0,"C");
      $pdf->Cell(36.5,5,"Payments",1,0,"C");
     $pdf->Cell(26.5,5,"OR #",1,0,"C");
     $pdf->Cell(36.5,5,"Client",1,0,"C");
     $pdf->Cell(36.5,5,"Property",1,0,"C");

     $pdf->Cell(56.5,5,"Description",1,1,"C");
     $pdf->SetFont('Times','',10);

          $sql3 = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`,  prop.`propertyName`, plist.`block`, plist.`lot`, pay.`payment`, pay.`paymentDesc` FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
 AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND pay.`paymentMethod`='Bank' AND (pay.`created_at` BETWEEN '".$_GET['from']."' AND '".$_GET['to']."');
 ";
             $result1 = mysqli_query($conn, $sql3);
             $n1=0;
             $bank1=0;
             $cash1=0;
             $total1=0;
             $repeat1='';
          if (mysqli_num_rows($result1) > 0) {
             while($row1 = mysqli_fetch_assoc($result1)) {



                 $total1= $total1 + $row1["payment"];



                 $n1++;
                 $pdf->Cell(5,5,$n1,0,0,"C");
                 $pdf->Cell(36.5,5,number_format(($row1["payment"]),2,'.',','),0,0,"C");
                 $pdf->Cell(26.5,5,$row1["or_num"],0,0,"C");
                 $pdf->Cell(36.5,5,$row1["rname"],0,0,"C");
                 $pdf->Cell(36.5,5,$row1["propertyName"] ,0,0,"C");

                 $pdf->Cell(56.5,5,$row1["paymentDesc"],0,1,"C");




                 }

   $pdf->Ln(10);


          } else {
              $pdf->Cell(197.5,5,"NO BANK PAYMENTS!!",0,1,"C");
          }
            $pdf->Ln(2);
           $pdf->SetFont('Times','B',10);
         $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(26.5,5,"",0,0,"C");
    $pdf->Cell(56.5,5,"",0,0,"C");

    $pdf->Cell(36.5,5,"",0,0,"C");
      $pdf->SetFont('Times','',15);

    $pdf->Cell(73,5,"BANK TOTAL: ".number_format(round($total1,2),2,'.',',')." "  ,0,1,"C");
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(0,5,'EXPENSE:',0,1,'L');
    $pdf->Ln(2);
    $pdf->SetFont('Times','',10);


  $pdf->Cell(5,5,"#",1,0,"C");
      $pdf->Cell(30,5,"Expense For",1,0,"C");
      $pdf->Cell(20,5,"Amount",1,0,"C");
      $pdf->Cell(50.5,5,"Details",1,1,"C");
      $pdf->SetFont('Times','',10);

           $sql = "SELECT * FROM expenses WHERE  (`created_at` BETWEEN '".$_GET['from']."' AND '".$_GET['to']."');
  ";
              $result = mysqli_query($conn, $sql);
              $n2=0;

              $totalExpense=0;

           if (mysqli_num_rows($result) > 0) {
              while($row = mysqli_fetch_assoc($result)) {



                  $totalExpense= $totalExpense + $row["Amount"] ;



                  $n2++;
                  $pdf->Cell(5,5,$n2,0,0,"C");
                  $pdf->Cell(30,5,$row["ExpenseName"],0,0,"C");
                  $pdf->Cell(20,5,$row["Amount"],0,0,"C");
                  $pdf->Cell(50.5,5,$row["details"],0,1,"C");





                  }
                  //  $pdf->Cell(5,5,$n,0,0,"C");
                  // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                  // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                  // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                  // }
                  // else
                  // {
                  // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                  // }

                  // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                  // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                  // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                  // $pdf->Cell(26,5,'',0,1,"C");



           } else {
               $pdf->Cell(105.5,5,"NO EXPENSE RECORDED!!",0,1,"C");
           }
             $pdf->Ln(10);
           $pdf->SetFont('Times','B',10);
           $pdf->Cell(5,5,"",0,0,"C");
           $pdf->Cell(30,5,"",0,0,"C");
              $pdf->SetFont('Times','',15);
           $pdf->Cell(70,5,"Total Expense: " . number_format(round($totalExpense,2),2,'.',',')." "  ,0,1,"C");


           $pdf->SetFont('Times','B',20);
           $pdf->Ln(20);
           $cashtotal = $total - $totalExpense;
           $pdf->Cell(100,10,"Cash On Hand: " .  number_format(round($cashtotal,2),2,'.',',')." "  ,0,0,"C");

         mysqli_close($conn);

$pdf->Output();
?>
