<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');
    $this->Ln(13);
    $this->Image('logo-invert.jpg',95,5,-1500);
    $this->setFont("Arial",'B',8);
    $this->Cell(201,3,"DSR IT SOLUTIONS.",0,1,"C");
     $this->setFont("Arial",'',8);
    $this->Cell(201,3,"Door 4 C Bldg. LS Violan & Sons Inc.",0,1,"C");
    $this->Cell(201,3,"F Inigo St., Brgy. 3-A, Poblacion District,",0,1,"C");
    $this->Cell(201,3,"Davao City, Davao del Sur, Philippines 8000",0,1,"C");
    $this->Cell(201,3,"Tel no. (082) 322-6819",0,1,"C");
     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(4);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'DSR IT SOLUTIONS.',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '2n.QL8(TeB}qGk?4';//2n.QL8(TeB}qGk?4 for testing live
         $dbname = 'diamante_db';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }

$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"CLIENT'S NAME: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,"COURTLAND REALTY SERVICES",0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ADDRESS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,"Purok Puso, Cabaluna Street, Barangay Gredu, Panabo City, 8015, Davao del Norte",0,1,"L");
  $pdf->Ln(2);
$pdf->SetFont('Times','B',10);



$pdf->SetFont('Times','B',10);

        $pdf->SetFont('Times','B',10);
$pdf->Cell(5,5,"#",1,0,"C");
    $pdf->Cell(20,5,"OR NUM",1,0,"C");
    $pdf->Cell(55,5,"Transaction for",1,0,"C");
    $pdf->Cell(45,5,"Property",1,0,"C");
    $pdf->Cell(30,5,"Payment",1,0,"C");
    $pdf->Cell(30,5,"Commission",1,1,"C");


         $sql = "SELECT pay.`paymentDesc`, pay.`or_num`, prop.`propertyName`, pl.`block`, pl.`lot`, pay.`systemPay1`,pay.`created_at`, pay.`systemPay2`,pay.`isBilled1`,pay.`isBilled2`
FROM payments pay, propertylists pl, properties prop, client__properties cp
WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid` = pl.`propertylistid` AND pl.`propId`=prop.`propId` AND pay.`isActive`='1' AND (pay.`isBilled1`='0' OR pay.`isBilled2`='0') ";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {



                $total= $total + $row["systemPay1"] + $row["systemPay2"];

                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                $pdf->Cell(20,5,$row["or_num"],0,0,"C");
                $pdf->Cell(55,5,$row["paymentDesc"],0,0,"C");
                $pdf->Cell(45,5,$row["propertyName"],0,0,"C");
                $pdf->Cell(30,5,number_format(($row["systemPay1"]),2,'.',','),0,0,"C");
                $pdf->Cell(30,5,number_format(($row["systemPay2"]),2,'.',','),0,1,"C");




                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(196,5,"NO PAYMENTS YET!!",1,1,"C");
         }
         $pdf->Ln(2);
           $pdf->SetFont('Times','B',10);

    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(85,5,"",0,0,"C");
    $pdf->Cell(80,5,"TOTAL BILL: ".number_format(round($total,2),2,'.',',')." "  ,1,1,"C");


         mysqli_close($conn);

$pdf->Output();
?>
